<?php

namespace Procontext\Mailer;

use \PDO;
use \Exception;
use Procontext\Mailer\Exception\DBConnectionException;
use Procontext\Mailer\Exception\EmailNotExistException;
use Procontext\Mailer\Exception\PasswordNotExistException;

class QueueSender
{
    const PASSWORD_ENCRYPTION_KEY = '#4ewr%*&()WE3dR@#!@#$^_(*&4wd%$d2';

    private $pdo;

    public function __construct()
    {
        $host = env('MAILER_HOST', '127.0.0.1');
        $port = env('MAILER_PORT', '3306');
        $db   = env('MAILER_DB');
        $user = env('MAILER_USERNAME');
        $password = env('MAILER_PASSWORD');

        $charset = 'utf8';
        $dsn = "mysql:host=$host;dbname=$db;port=$port;charset=$charset";
        try {
            $this->pdo = new PDO($dsn, $user, $password);
        } catch (Exception $exception) {
            throw new DBConnectionException();
        }
    }

    public function send(string $title, string $subject, string $body, array $recipients): bool
    {
        $configId = $this->getConfigId();

        $stmt = $this->pdo->prepare("INSERT INTO mails (config,title,subject,recipients,body) VALUES (:config,:title,:subject,:recipients,:body)");
        return $stmt->execute(
            [
                'config' => $configId,
                'title' => $title,
                'subject' => $subject,
                'recipients' => json_encode($recipients,JSON_UNESCAPED_UNICODE),
                'body' => $body
            ]
        );
    }

    private function getConfigId()
    {
        $email = env('MAIL_EMAIL');
        $password = env('MAIL_PASSWORD');

        if(!$email) {
            throw new EmailNotExistException();
        }

        if(!$password) {
            throw new PasswordNotExistException();
        }

        $stmt = $this->pdo->prepare("SELECT * FROM configs WHERE email=:email");
        $stmt->execute(['email' => $email]);

        if($config = $stmt->fetchObject()) {
            $emailPassword = $this->decryptPassword($config->password);
            if($emailPassword != $password) {
                $stmt = $this->pdo->prepare("UPDATE configs SET password=:password WHERE id=:id");
                $stmt->execute([
                    'id' => $config->id,
                    'password' => $this->encryptPassword($password)
                ]);
            }
        } else {
            $stmt = $this->pdo->prepare("INSERT INTO configs (email,password) VALUES (:email,:password)");
            $stmt->execute([
               'email' => $email,
               'password' => $this->encryptPassword($password)
            ]);
            $stmt = $this->pdo->prepare("SELECT * FROM configs WHERE email=:email");
            $stmt->execute(['email' => $email]);
            $config = $stmt->fetchObject();
        }
        return $config->id;
    }

    private function encryptPassword($string)
    {
        $ivlen = openssl_cipher_iv_length($cipher="AES-128-CBC");
        $iv = openssl_random_pseudo_bytes($ivlen);
        $ciphertext_raw = openssl_encrypt($string, $cipher, self::PASSWORD_ENCRYPTION_KEY, $options=OPENSSL_RAW_DATA, $iv);
        $hmac = hash_hmac('sha256', $ciphertext_raw, self::PASSWORD_ENCRYPTION_KEY, $as_binary=true);
        return base64_encode( $iv.$hmac.$ciphertext_raw );
    }

    private function decryptPassword($string)
    {
        $c = base64_decode($string);
        $ivlen = openssl_cipher_iv_length($cipher="AES-128-CBC");
        $iv = substr($c, 0, $ivlen);
        $sha2len=32;
        $ciphertext_raw = substr($c, $ivlen+$sha2len);
        return openssl_decrypt($ciphertext_raw, $cipher, self::PASSWORD_ENCRYPTION_KEY, $options=OPENSSL_RAW_DATA, $iv);
    }
}