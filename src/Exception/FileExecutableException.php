<?php

namespace Procontext\Mailer\Exception;

use Throwable;

class FileExecutableException extends MailerException
{
    public function __construct($message = "Запрещено прикреплять исполняемые файлы", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}