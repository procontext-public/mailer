<?php

namespace Procontext\Mailer\Exception;

use Throwable;

class DBConnectionException extends MailerException
{
    public function __construct($message = "Проблемы соединения с базой данных", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}