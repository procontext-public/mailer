<?php

namespace Procontext\Mailer\Exception;

use Throwable;

class PasswordNotExistException extends MailerException
{
    public function __construct($message = "Пароль для почты не задан", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}