<?php

namespace Procontext\Mailer\Exception;

use Throwable;

class FileNotExistException extends MailerException
{
    public function __construct($message = "Файл не найден", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}