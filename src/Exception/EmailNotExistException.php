<?php

namespace Procontext\Mailer\Exception;

use Throwable;

class EmailNotExistException extends MailerException
{
    public function __construct($message = "Email не задан", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}