<?php

namespace Procontext\Mailer\Exception;

use Throwable;

class ModeException extends MailerException
{
    public function __construct($message = "Не корректный режим отправки", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}