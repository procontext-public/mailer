<?php

namespace Procontext\Mailer;

use Procontext\Mailer\Exception\EmailNotExistException;
use Procontext\Mailer\Exception\FileExecutableException;
use Procontext\Mailer\Exception\FileNotExistException;
use Procontext\Mailer\Exception\PasswordNotExistException;
use Swift_Attachment;
use Swift_SmtpTransport;
use Swift_Mailer;
use Swift_Message;

class SyncSender
{
    private Swift_Mailer $mailer;
    private $email;

    /**
     * @throws PasswordNotExistException
     * @throws EmailNotExistException
     */
    public function __construct()
    {
        $host = env('MAIL_HOST', 'smtp.yandex.ru');
        $port = env('MAIL_PORT', 465);
        $encryption = env('MAIL_ENCRYPTION', 'ssl');
        $email = env('MAIL_EMAIL');
        $password = env('MAIL_PASSWORD');

        if(!$email) {
            throw new EmailNotExistException();
        }

        if(!$password) {
            throw new PasswordNotExistException();
        }

        $transport = new Swift_SmtpTransport($host, $port, $encryption);

        if($encryption == "tls"){
            $transport->setUsername($email)
                ->setPassword($password)
                ->setStreamOptions(array('ssl' => array('allow_self_signed' => true, 'verify_peer' => false)));
        }else{
            $transport->setUsername($email)
                ->setPassword($password);
        }

        $this->email = $email;
        $this->mailer = new Swift_Mailer($transport);
    }

    /**
     * @param string $title
     * @param string $subject
     * @param string $body
     * @param array $recipients
     * @param array $files
     * @return bool
     * @throws FileExecutableException
     * @throws FileNotExistException
     */
    public function send(string $title, string $subject, string $body, array $recipients, array $files = []): bool
    {
        $message = new Swift_Message();
        $message->setFrom($this->email, $title);
        $message->setTo($recipients);
        $message->setSubject($subject);

        if($files) {
            foreach($files as $file) {
                if(!file_exists($file)) throw new FileNotExistException();
                if(is_executable($file)) throw new FileExecutableException();
                $message->attach(Swift_Attachment::fromPath($file));
            }
        }

        $message->setBody($body, 'text/html');

        return (bool) $this->mailer->send($message);
    }
}
