<?php

namespace Procontext\Mailer;

use Procontext\Mailer\Exception\AdminEmailNotExistException;
use Procontext\Mailer\Exception\FileExecutableException;
use Procontext\Mailer\Exception\FileNotExistException;
use Procontext\Mailer\Exception\ModeException;

class Mailer
{
    const MODE_SYNC = 'sync';
    const MODE_QUEUE = 'queue';

    private $url;
    private $title;
    private $mode;
    private $adminEmail;

    public function __construct()
    {
        $this->url = env('URL' , '');
        $this->title = env('MAIL_TITLE', null) ?: 'LandingPage ' . $this->url;
        $this->mode = env('MAIL_MODE', self::MODE_SYNC);
        $this->adminEmail = env('ADMIN');
    }

    /**
     * @throws AdminEmailNotExistException
     * @throws FileExecutableException
     * @throws FileNotExistException
     * @throws ModeException
     */
    public function send(string $body, string $subject = '', array $files = []): bool
    {
        $subject = $subject ?: 'Запрос обратного звонка ' . $this->url;

        $recipientsEmails = env('MAIL_RECIPIENTS', '');

        if (!$this->adminEmail) {
            throw new AdminEmailNotExistException();
        }

        $recipients = array_merge($recipientsEmails, [$this->adminEmail => '']);

        switch ($this->mode) {
            case self::MODE_SYNC:
                $sender = new SyncSender();
                break;
            case self::MODE_QUEUE:
                $sender = new QueueSender();
                break;
            default:
                throw new ModeException();
        }

        return $sender->send($this->title, $subject, $body, $recipients, $files);
    }

    /**
     * @throws AdminEmailNotExistException
     */
    public function sendAlert(string $body, array $additionalRecipients = []): bool
    {
        $subject = 'Ошибка на лендинге ' . $this->url;

        if (!$this->adminEmail) {
            throw new AdminEmailNotExistException();
        }

        $recipients = array_merge($additionalRecipients, [$this->adminEmail => '']);

        $sender = new SyncSender();
        return $sender->send($this->title, $subject, $body, $recipients);
    }
}